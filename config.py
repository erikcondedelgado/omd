class Config():
    """
    Thisis basic Config class. In this class could be define
    some global variables.
    """

    STRAWBERRY_LABEL_ERROR_MSG = "Error: Not found"
    DATABASE_PATH = "database/database.json"
