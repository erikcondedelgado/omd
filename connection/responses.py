import strawberry


@strawberry.type
class Response:
    status: str
    message: str
