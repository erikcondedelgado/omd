from typing import Dict

def work_center_id_exist(data: Dict, id: int) -> bool:
    for item in data["work_center"]:
        if item['id'] == id:
            return True

def calendar_id_exist(data: Dict, id: int) -> bool:
    for item in data["calendar"]:
        if item['id'] == id:
            return True