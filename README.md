# OMD FastAPI + Strawberry project

## Getting started

First step to run this project is to install some dependencies following next command:
```bash
pip install -r requirements.txt
```

## Running project
To run this project you should run following command: 

```bash
uvicorn main:app --reload
```

## Testing the project
To test the project you should go to yout browser and type: http://127.0.0.1:8000/graphql
Then, when GraphiQL terminal is shown, you can write differente queries to test the code.
Here you can see four examples:

### 1. Get WorkCenter test example
```graphql
{
  getWorkCenter(id: 1) {
    id
    name
    address1
    address2
    organization
    calendar {
      id
      year
      workOnHollidays
    }
  }
}
```

### 2. Get Calendar test example
```graphql
{
  getCalendar(id: 1) {
    id
    year
    workOnHollidays
    workCenter {
      id
      name
      address1
      address2
      organization
    }
  }
}
```

### 3. Set WorkCenter teste xample
```graphql
mutation {
    setWorkCenter (id: 5, name: "New work center", address1: "New address 1", address2: "New address 2", calendarIds: [1, 2, 3], organization: 1){
	status
      	message
    }
}
```

### 4. Set Calendar test example

```graphql
mutation {
    setCalendar (id: 5, workCenterId: 1, year: 2024, workOnHollidays: true){
    	status
        message
    }
}

```

## Some important notes
In this code I only create four differents functions, two of that in charge to get some data from WorkCenter and Calendar objects and the oter two in charge of create new entries in the database. 
In each functions user should introduce a parameter (id) to return a specific data or to create new entry with desired ID.
I've decided to implement a kind of CRUD, but only considering the creation and retrieval of data, since the update and deletion processes would be very similar to the creation process. I would only need to update the dictionary or delete entries from it

## How to launch Pytest?
In this project there are some tests to check different functionality of the code. Testing files are located in test directory and here, you can find test files.
To execute pytest you should go to your terminal and type following command:

```bash
pytest .\test
```

If you only want to launch tests related to an specific file, you should launch following command:

```bash
pytest .\test\test_file.py
```