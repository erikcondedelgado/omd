import pytest
from typing import Dict

from functions import work_center_id_exist, calendar_id_exist


@pytest.fixture
def sample_data() -> Dict:
    """
    This function is in charge of create basic database data

    :return: Database data dict
    """

    return {
        "work_center": [{"id": 1, "name": "Work Center 1"}, {"id": 2, "name": "Work Center 2"}],
        "calendar": [{"id": 10, "name": "Calendar 1"}, {"id": 20, "name": "Calendar 2"}]
    }

def test_work_center_id_exist(sample_data):
    """
    This function is in charge of test two different posibilities:
        1. Check if workCenter exists and return True
        2. Check if workCenter doesn't exist and return False

    :param sample_data: Basic database dictionary sample
    """

    assert work_center_id_exist(sample_data, 1) == True
    assert work_center_id_exist(sample_data, 3) == None

def test_calendar_id_exist(sample_data):
    """
    This function is in charge of test two different posibilities:
        1. Check if Calendar exists and return True
        2. Check if Calendar doesn't exist and return False

    :param sample_data: Basic database dictionary sample
    """

    assert calendar_id_exist(sample_data, 10) == True
    assert calendar_id_exist(sample_data, 30) == None
