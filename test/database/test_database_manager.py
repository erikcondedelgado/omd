import pytest
import json

from database.database_manager import DatabaseManager, DatabaseManagerException


@pytest.fixture
def temp_file(tmpdir) -> str:
    """
    With this function we can create and store database in a
    new temporary location.

    :param tmpdir: Pytest ficture parameter, this parameter
                   creates a new temporary directory
    :return: String with file path
    """

    file = tmpdir.join("database.json")
    data = {"data": "example"}
    with open(file, "w") as f:
        json.dump(data, f)
    return str(file)

def test_read_database(temp_file):
    """
    This test read file from the path and check if data inside
    is {"data": "example"}

    :param temp_file: Function to create enw temporary file
    """

    db_manager = DatabaseManager(temp_file)
    data = db_manager.read_database()
    assert data == {"data": "example"}

def test_read_database_false_file():
    """
    This test read file from inexistent path and check if there's
    a DatabaseManagerException

    :raises DatabaseManagerException if file not exist
    """

    db_manager = DatabaseManager("false_file.json")
    with pytest.raises(DatabaseManagerException):
        db_manager.read_database()
