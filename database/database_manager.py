from typing import Dict
import json


class DatabaseManager:

    def __init__(self, path: Dict):
        self._path = path

    def read_database(self):
        try:
            with open(self._path, 'r') as f:
                data = json.load(f)
            return data

        except FileNotFoundError as error:
            raise DatabaseManagerException(error)


class DatabaseManagerException(Exception):

    def __init__(self, error: str):
        assert error
