import logging

import strawberry
from typing import List, Optional

from config import Config
from database.database_manager import DatabaseManager
from functions import work_center_id_exist, calendar_id_exist
from connection.responses import Response

error_msg = Config.STRAWBERRY_LABEL_ERROR_MSG
data = DatabaseManager(Config.DATABASE_PATH).read_database()
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')


@strawberry.type
class WorkCenter:
    id: int
    name: str
    address1: str
    address2: str
    calendarIds: Optional[List[int]] = None
    organization: int

    @classmethod
    def get_data(cls, data: dict) -> 'WorkCenter':
        """
        This function is in charge to return WorkCenter object data
        from a simple query.

        :param data: Item data
        :return: WorkCenter type object
        """

        return cls(
            id=data.get("id", error_msg),
            name=data.get("name", error_msg),
            address1=data.get("address1", error_msg),
            address2=data.get("address2", error_msg),
            calendarIds=data.get('calendarIds', error_msg),
            organization=data.get("organization", error_msg),
        )

    @strawberry.field
    def calendar(self) -> List['Calendar']:
        """
        This function is a Strawberry field. This is in charge of define an specific
        field of WorkCenter object. In this case the field that we want to work with
        is calendar.
        This function get the calendar list of IDs and put the Calendar objects, if
        that exists, in WorkCenter object calendar field.

        :return: Calendar object list
        """

        if self.calendarIds:
            calendars = [Query().get_calendar(id) for id in self.calendarIds]
            return [calendar for calendar in calendars if calendar is not None]
        return []


@strawberry.type
class Calendar:
    id: int
    workCenterId: Optional[int] = None
    year: int
    workOnHollidays: bool

    @classmethod
    def get_data(cls, data: dict) -> 'Calendar':
        """
        This function is in charge to return Calendar object data
        from a simple query.

        :param data: Item data
        :return: WorkCenter type object
        """

        return cls(
            id=data.get("id", error_msg),
            workCenterId=data.get('workCenterId'),
            year=data.get("year", error_msg),
            workOnHollidays=data.get("workOnHollidays", error_msg),
        )

    @strawberry.field
    def work_center(self) -> Optional['WorkCenter']:
        """
        This function is a Strawberry field. This is in charge of define an specific
        field of Calendar object. In this case the field that we want to work with
        is work_center.
        This function get the work center ID and put the WorkCenter object, if
        that exists, in Calendar object work_center field.

        :return: WorkCenter object
        """

        if self.workCenterId:
            return Query().get_work_center(self.workCenterId)
        return None


@strawberry.type
class Query:
    @strawberry.field
    def get_work_center(self, id: int) -> Optional[WorkCenter]:
        """
        This function work as getter and return work center if it exists,
        else it return null.

        :param id: ID of WorkCenter to return
        :return: WorkCenter object
        """

        logging.info(f"Returning WorkCenter with ID '{id}'")

        for item in data["work_center"]:
            if item["id"] == id:
                return WorkCenter.get_data(item)
        return None

    @strawberry.field
    def get_calendar(self, id: int) -> Optional[Calendar]:
        """
        This function work as getter and return calendar if it exists,
        else it return null.

        :param id: ID of Calendar to return
        :return: Calendar object
        """

        logging.info(f"Returning Calendar with ID '{id}'")

        calendar = next((Calendar.get_data(item) for item in data["calendar"] if item["id"] == id), None)
        return calendar


@strawberry.type
class Mutation:

    @strawberry.mutation
    def set_work_center(self, id: int, name: str, address1: str, address2: str, calendar_ids: List[int],
                        organization: int) -> Response:

        """
        This function work as setter and return Response success message
        if new work center sucessfully added and error message if failed.

        :param
            id: ID of new WorkCenter
            name: Name of WorkCenter
            address1: First address of WorkCenter
            address2: Second address of WorkCenter
            calendar_ids: WorkCenter related calendar ids
            Organization: WorkCenter organization

        :return: Response object
        """

        new_work_center = {
            "id": id,
            "name": name,
            "address1": address1,
            "address2": address2,
            "calendarIds": calendar_ids,
            "organization": organization,
        }

        if work_center_id_exist(data, id):
            msg = f"Work Center with ID '{id}' already exist"
            logging.info(msg)

            return Response(status="Failed", message=msg)

        try:
            data['work_center'].append(new_work_center)

            msg = f"Work center with ID '{id}' successfully added"
            logging.info(msg)

            return Response(status="Success", message=msg)

        except Exception as e:
            logging.error(str(e))
            return Response(status="Failed", message=str(e))

    @strawberry.mutation
    def set_calendar(self, id: int, work_center_id: int, year: int, work_on_hollidays: bool) -> Response:

        """
        This function work as setter and return Response success message
        if new calendar sucessfully added and error message if failed.

        :param
            id: ID of new Calendar
            work_center_id: ID of related work center
            year: Year of calendar
            work_on_holliday: True or False depends on there's work in holliday or not

        :return: Response object
        """

        new_calendar = {
            "id": id,
            "workCenterId": work_center_id,
            "year": year,
            "workOnHollidays": work_on_hollidays,
        }

        if calendar_id_exist(data, id):
            msg = f"Calendar with ID '{id}' already exist"
            logging.info(msg)

            return Response(status="Failed", message=msg)

        try:
            data['calendar'].append(new_calendar)

            msg = f"Calendar with ID '{id}' successfully added"
            logging.info(msg)

            return Response(status="Success", message=msg)

        except Exception as e:
            logging.error(str(e))
            return Response(status="Failed", message=str(e))
